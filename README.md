# <span style="color: #9FCAF4;">Searchiom Engine</span>

## Dataset Used

### <span style="color: #f49fca;">First Dataset</span>

- Name : **AQUAINT**
- Link : [aquaint - TREC Robust 2005](https://ir-datasets.com/aquaint.html#aquaint/trec-robust-2005)
- Data Size : 1033461


### <span style="color: #f49fca;">Second Dataset</span>

- Name:  **antique**
- Link: [aquaint - TREC Robust 2005](https://ir-datasets.com/aquaint.html#aquaint/trec-robust-2005)
- Data Size: 403666

----

### BM25 Algorithm 

![Alt Text](score_bm25.svg)

![Alt Text](IDF.svg)

------

# Note
    To run application make virtual Environment
    and install requiremnt.txt

## Note 
    make install to requirements to update packages


## to run server fast api  
    uvicorn main:app --reload


