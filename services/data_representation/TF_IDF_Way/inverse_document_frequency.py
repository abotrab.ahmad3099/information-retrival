import math
from search_engine.services.data_representation.create_inverted_index import create_inverted_index

print(create_inverted_index('ahmad'))


def calculate_idf(corpus):
    idf = {}
    n_docs = len(corpus)
    inverted_index = create_inverted_index(corpus)
    for term, doc_ids in inverted_index.items():
        idf[term] = math.log(n_docs / len(doc_ids))
    return idf
