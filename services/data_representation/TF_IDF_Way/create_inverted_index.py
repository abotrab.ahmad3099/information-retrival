import nltk
import sys
from collections import defaultdict


def create_inverted_index(data):
    return data
    inverted_index = defaultdict(list)
    for doc_id, doc_content in data.items():
        ## add some text processing , Tokenize, remove stop words ... to create a list of cleaned terms
        ## write your code here
        terms = nltk.word_tokenize(doc_content)
        for term in terms:
            inverted_index[term].append(doc_id)
    return dict(inverted_index)


sys.modules[__name__] = create_inverted_index
