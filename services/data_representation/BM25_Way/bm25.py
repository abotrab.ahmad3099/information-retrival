from collections import defaultdict

from rank_bm25 import BM25Okapi

import search_engine.services.data_processing.data_rooting as dpr
import search_engine.services.data_processing.data_loading as dpl
import search_engine.services.data_processing.data_cleaning as dpcl


def representation_data(dataset_name, query):
    i = 0
    corpus = []
    tokenized_corpus = []

    ds = dpl.get_dataset(dataset_name)
    inverted_index = defaultdict(list)

    for d in ds.docs_iter():
        if i < 10:
            text = dpcl.remove_whitespace(d.text)
            corpus.append(text)
            word_token = dpr.get_word_token(text)
            word_token_with_out_stop_word = dpcl.remove_stop_word_from_list(word_token)
            word_token_with_out_punctuation_mark = dpcl.remove_punctuation_from_list(word_token_with_out_stop_word)
            tokenized_corpus.append(word_token_with_out_punctuation_mark)

            i += 1
        else:
            break

    bm25 = BM25Okapi(tokenized_corpus)

    query = query
    tokenize_query = dpcl.remove_whitespace(query)
    tokenize_query = dpr.get_word_token(tokenize_query)
    tokenize_query = dpcl.remove_stop_word_from_list(tokenize_query)
    tokenize_query = dpcl.remove_punctuation_from_list(tokenize_query)

    doc_scores = bm25.get_scores(tokenize_query)

    result = bm25.get_top_n(tokenize_query, corpus, 1)

    return result
    # print(result)



# test command
#

# for doc in corpus:
#     print(doc)
#     print()

# print(doc_scores)

# return dict(inverted_index)
