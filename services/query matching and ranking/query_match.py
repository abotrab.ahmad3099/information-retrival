import math
from collections import Counter


class Query_match_and_ranking:
    def __init__(self, documents):
        self.documents = documents
        self.document_length_avg = sum(len(doc.split()) for doc in documents) / len(documents)
        self.doc_freqs = self.calculate_doc_frequencies()
        self.k1 = 1.2
        self.b = 0.75

    def calculate_doc_frequencies(self):
        doc_freqs = Counter()
        for doc in self.documents:
            terms = set(doc.split())
            doc_freqs.update(terms)
        return doc_freqs

    def calculate_score(self, query):
        scores = {}
        query_terms = query.split()
        query_term_freqs = Counter(query_terms)
        for doc_id, doc in enumerate(self.documents):
            doc_terms = doc.split()
            doc_length = len(doc_terms)
            doc_term_freqs = Counter(doc_terms)
            doc_score = 0
            for term in query_terms:
                if term in doc_term_freqs:
                    idf = math.log((len(self.documents) - self.doc_freqs[term] + 0.5) / (self.doc_freqs[term] + 0.5))
                    tf = doc_term_freqs[term] * (self.k1 + 1) / (doc_term_freqs[term] + self.k1 * (
                            1 - self.b + self.b * doc_length / self.document_length_avg))
                    query_weight = (query_term_freqs[term] * (1.2 + 1)) / (query_term_freqs[term] + 1.2)
                    doc_score += idf * tf * query_weight
            scores[doc_id] = doc_score
        return scores


# Example usage
documents = [
    "This is the first document",
    "This document is the second document",
    "And this is the third one",
    "Is this the first document?",
]

bm25 = Query_match_and_ranking(documents)

query = "first document"
scores = bm25.calculate_score(query)

# Sort the documents based on scores
sorted_docs = sorted(scores.items(), key=lambda x: x[1], reverse=True)

# Print the sorted documents with scores
for doc_id, score in sorted_docs:
    print(f"Document ID: {doc_id}, Score: {score}")
