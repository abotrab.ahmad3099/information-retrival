from nltk.stem.porter import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.stem import WordNetLemmatizer

stemmer = PorterStemmer()
lemmatizer = WordNetLemmatizer()


def get_word_token(text): return word_tokenize(text)


def stem_words(text):
    word_tokens = get_word_token(text)
    stems = [stemmer.stem(word) for word in word_tokens]
    return stems

def stem_one_words(text):
    return stemmer.stem(text)


def stem_dataset(dataset):
    for query in dataset.queries_iter():
        print(stem_words(query))


def lemmatize_one_word(text):
    return lemmatizer.lemmatize(text)
