import string
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize


def remove_stop_word_from_list(list):
    new_list = []
    for w in list:
        if not is_stop_word(w):
            new_list.append(w)
    return new_list


def remove_stop_words(text):
    stop_words = set(stopwords.words('english'))

    word_tokens = word_tokenize(text)

    filtered_sentence = [w for w in word_tokens if not w.lower() in stop_words]

    for w in word_tokens:
        if w not in stop_words:
            filtered_sentence.append(w)

    return filtered_sentence


def is_stop_word(text):
    stop_words = set(stopwords.words('english'))
    return text in stop_words


def remove_punctuation(text):
    translator = str.maketrans('', '', string.punctuation)
    return text.translate(translator)


def remove_punctuation_from_list(list):
    new_list = []
    for w in list:
        if not is_punctuation_word(w):
            new_list.append(w)
    return new_list


def is_punctuation_word(text):
    return text in string.punctuation


def remove_whitespace(text):
    return " ".join(text.split())
