from collections import defaultdict

import search_engine.services.data_processing.data_rooting as dpr
import search_engine.services.data_processing.data_loading as dpl
import search_engine.services.data_processing.data_cleaning as dpcl


def main():
    ds = dpl.get_dataset("antique/train")
    corpus = []
    i=0

    # for doc_id, doc_content in corpus.items():
    #     ## add some text processing , Tokenize, remove stop words ... to create a list of cleaned terms
    #     ## write your code here
    #     terms = nltk.word_tokenize(doc_content)
    #
    #     # print(terms)
    #
    #     for term in terms:
    #         inverted_index[term].append(doc_id)
    #
    # print(dict(inverted_index))
    # return dict(inverted_index)

    inverted_index = defaultdict(list)

    for d in ds.docs_iter():
        if(i<10):
            text = dpcl.remove_whitespace(d.text)
            word_token = dpr.get_word_token(text)
            word_token_with_out_stop_word = dpcl.remove_stop_word_from_list(word_token)
            word_token_with_out_punctuation_mark = dpcl.remove_punctuation_from_list(word_token_with_out_stop_word)

            for term in word_token_with_out_punctuation_mark:
                inverted_index[term].append(d.doc_id)
            i += 1
        else:
            break
    return dict(inverted_index)

    # for i in range(0, len(word_token_with_out_punctuation_mark)):
    #     word_token_with_out_punctuation_mark[i] = dpr.stem_one_words(word_token_with_out_punctuation_mark[i])
    #     word_token_with_out_punctuation_mark[i] = dpr.lemmatize_one_word(word_token_with_out_punctuation_mark[i])
    # return corpus


if __name__ == "__main__":
    words = main()

    print(words)
    # for word in words:
    #    print(word)
