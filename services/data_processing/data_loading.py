import nltk as nl
import ir_datasets as ir


def get_dataset(data_set_name):
    dataset = ir.load(data_set_name)
    return dataset
